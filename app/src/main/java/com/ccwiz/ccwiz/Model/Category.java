package com.ccwiz.ccwiz.Model;

import com.ccwiz.ccwiz.Utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Enver on 22.03.2017..
 */

public class Category {

    public int Id;
    public String Title;
    public boolean Enabled;
    public CategoryType type;

    public static Category parseJSON(String response) {
        Gson gson = new GsonBuilder().registerTypeAdapter(boolean.class, Utils.booleanAsIntAdapter).create();
        return gson.fromJson(response, Category.class);
    }

    @Override
    public String toString() {
        return Title;
    }
}
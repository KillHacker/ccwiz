package com.ccwiz.ccwiz.Model;

import com.ccwiz.ccwiz.Utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.sql.Timestamp;

/**
 * Created by Enver on 21.03.2017..
 */

public class User {

    public int Id;
    public String ApiKey;
    public String Email;
    public String Username;
    public int AccessLevel;
    public Timestamp LastOnlineTimestamp;
    public boolean TrackLocation;
    public boolean SaveMessages;
    public boolean LogCalls;
    public int MaxDifficultyLevel;
    public int QuestionCount;
    public int Score;
    public int AverageTime;
    public int OnlineAgo;

    public static User parseJSON(String response) {
        Gson gson = new GsonBuilder().registerTypeAdapter(boolean.class, Utils.booleanAsIntAdapter).registerTypeAdapter(Timestamp.class, Utils.timestampAsStringAdapter).create();
        return gson.fromJson(response, User.class);
    }

    public String getApiKey() {
        return ApiKey;
        //return "a6fde4604cf5dbbb7310379849ceac3e";
    }
}

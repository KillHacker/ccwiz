package com.ccwiz.ccwiz.Model;

import com.ccwiz.ccwiz.Utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Enver on 23.03.2017..
 */

public class Question {

    public int Id;
    public int CategoryId;
    public int ModelId;
    public int TechnologyId;
    public int DifficultyLevel;
    public String QuestionText;
    public String Option1;
    public String Option2;
    public String Option3;
    public String Option4;
    public int RightOption;
    public String DateAdded;
    public String DateModified;

    @SerializedName("Title")
    public String CategoryTitle;

    public static Question parseJSON(String response) {
        Gson gson = new GsonBuilder().registerTypeAdapter(boolean.class, Utils.booleanAsIntAdapter).create();
        Question question = gson.fromJson(response, Question.class);

        if (question.Option1 == null || question.Option1.toLowerCase().equals("null"))
            question.Option1 = "";
        if (question.Option2 == null || question.Option2.toLowerCase().equals("null"))
            question.Option2 = "";
        if (question.Option3 == null || question.Option3.toLowerCase().equals("null"))
            question.Option3 = "";
        if (question.Option4 == null || question.Option4.toLowerCase().equals("null"))
            question.Option4 = "";

        return question;
    }

    public String getRightOption() {
        switch (RightOption) {
            case 0:
                return Option1;
            case 1:
                return Option2;
            case 2:
                return Option3;
            case 3:
                return Option4;
            default:
                return null;
        }
    }

    public int AnswersCount() {
        if (Option3.isEmpty())
            return 2;
        else if (Option4.isEmpty())
            return 3;
        else
            return 4;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Question))
            return false;

        return Id == ((Question) obj).Id;
    }
}

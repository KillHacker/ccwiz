package com.ccwiz.ccwiz.Model;

/**
 * Created by Enver on 29.03.2017..
 */

public enum CategoryType {
    Category,
    Model,
    Technology
}

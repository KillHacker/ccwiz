package com.ccwiz.ccwiz.app;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ccwiz.ccwiz.Activities.LoginActivity;
import com.ccwiz.ccwiz.Model.User;
import com.ccwiz.ccwiz.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Enver on 30.03.2017..
 */

public class PushBackgroundService extends Service {

    public int counter = 0;
    int notificationID = 43622;

    Context context;
    private Timer timer;

    public PushBackgroundService(Context context) {
        super();
        this.context = context;
        Log.i("HERE", "here I am!");
    }

    public PushBackgroundService() {
        this.context = this;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        startTimer();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("EXIT", "ondestroy!");
        Intent broadcastIntent = new Intent("com.ccwiz.ccwiz.PushRecognition.RestartPushService");
        sendBroadcast(broadcastIntent);
        stoptimertask();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartService = new Intent(getApplicationContext(),
                this.getClass());
        restartService.setPackage(getPackageName());
        PendingIntent restartServicePI = PendingIntent.getService(
                getApplicationContext(), 1, restartService,
                PendingIntent.FLAG_ONE_SHOT);

        //Restart the service once it has been killed android


        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 100, restartServicePI);

    }

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                User sad = AppController.getInstance().mUser;
                if (sad != null) {
                    checkForNewQuestions();
                }
            }
        }, 2000, AppController.UpdatePeriodMillis); //
    }

    public void checkForNewQuestions() {
        final SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("SharedPrefs", 0);

        if (!sharedPref.getBoolean("pushEnabled", true))
            return;

        StringRequest getRequest = new StringRequest(Request.Method.POST, AppController.ServerName + "getLastTimestamp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);

                            if (object.getBoolean("hasNewQuestions")) {
                                Timestamp timestamp = Timestamp.valueOf(object.getJSONObject("lastTimestamp").getString("DateAdded"));
                                String lastTimestampString = sharedPref.getString("lastTimestamp", "2010-01-01 00:00:00");
                                Timestamp lastTimestamp = Timestamp.valueOf(lastTimestampString);

                                if (timestamp.after(lastTimestamp)) {
                                    //Notify
                                    final NotificationManager mgr = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    PendingIntent activity = PendingIntent.getActivity(context, 0, intent, 0);

                                    Notification note = new Notification.Builder(getApplicationContext()).setContentTitle("Ccwiz").setContentText("New questions are there! Check them").setSmallIcon(R.drawable.ic_questions).setContentIntent(activity).build();
                                    // Hide the notification after its selected
                                    note.flags |= Notification.FLAG_AUTO_CANCEL;
                                    mgr.notify(notificationID, note);

                                    SharedPreferences.Editor editor = sharedPref.edit();
                                    editor.putString("lastTimestamp", timestamp.toString());
                                    editor.apply();
                                }
                            }

                            if (object.getBoolean("hasPushNotifications")) {
                                JSONObject pushObject = object.getJSONObject("lastPushNotification");
                                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("SharedPrefs", 0);

                                Timestamp timestamp = Timestamp.valueOf(pushObject.getString("DateAdded"));
                                Timestamp lastTimestamp = Timestamp.valueOf(sharedPref.getString("lastPushTimestamp", "2010-01-01 00:00:00"));

                                if (timestamp.after(lastTimestamp)) {
                                    //Notify
                                    final NotificationManager mgr = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    PendingIntent activity = PendingIntent.getActivity(context, 0, intent, 0);

                                    Notification note = new Notification.Builder(getApplicationContext()).setContentTitle(pushObject.getString("Title")).setContentText(pushObject.getString("Message")).setSmallIcon(R.drawable.ic_questions).setContentIntent(activity).build();

                                    // Hide the notification after its selected
                                    note.flags |= Notification.FLAG_AUTO_CANCEL;
                                    mgr.notify(notificationID, note);

                                    SharedPreferences.Editor editor = sharedPref.edit();
                                    editor.putString("lastPushTimestamp", timestamp.toString());
                                    editor.apply();
                                }
                            }
                        } catch (JSONException e) {
                            Log.e("Volley", e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e("Volley", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", AppController.getInstance().mUser.getApiKey());

                return params;
            }
        };

        Volley.newRequestQueue(getApplicationContext()).add(getRequest);
    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
package com.ccwiz.ccwiz.app;

import android.app.Application;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ccwiz.ccwiz.Interfaces.OnDoneListener;
import com.ccwiz.ccwiz.Model.User;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AppController extends Application {

    //public static final String ServerName = "http://192.168.0.105:80/ccwiz/v1/";
    //public static final String ServerName = "http://10.0.2.2:80/ccwiz/v1/";
    //public static final String ServerName = "https://durubedu.000webhostapp.com/v1/";
    public static final String ServerName = "http://city-cars.in/ccwiz/v1/";

    public static final int UpdatePeriodMillis = 10000; //60000 is a minute

    public static final String TAG = "Tag";
    private static AppController mInstance;
    public User mUser;
    public OnDoneListener onUserLoaded;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        SharedPreferences sharedPreferences = getSharedPreferences("SharedPrefs", 0);
        if (sharedPreferences.contains("mUser")) {
            mUser = User.parseJSON(sharedPreferences.getString("mUser", ""));
            GetAlreadyLoggedUser(mUser);
        }
    }

    void GetAlreadyLoggedUser(final User loggedUser) {
        StringRequest postRequest = new StringRequest(Request.Method.POST, AppController.ServerName + "getUser",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        try {
                            JSONObject object = new JSONObject(response);

                            if (object.getBoolean("error")) {
                                String message = object.getString("message");
                                Log.e("Volley", message);
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            } else {
                                AppController.getInstance().mUser = User.parseJSON(object.getJSONObject("user").toString());

                                SharedPreferences sharedPreferences = getSharedPreferences("SharedPrefs", 0);
                                SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
                                sharedPreferencesEditor.putString("mUser", object.getJSONObject("user").toString());
                                sharedPreferencesEditor.apply();

                                if (onUserLoaded != null)
                                    onUserLoaded.OnDone();
                            }
                        } catch (Exception e) {
                            Log.e("Volley", e.getMessage());
                            Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e("Volley", error.toString());
                        Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", loggedUser.getApiKey());

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(postRequest);
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        req.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}

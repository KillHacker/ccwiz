package com.ccwiz.ccwiz.Activities;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.ccwiz.ccwiz.R;

import java.util.ArrayList;
import java.util.List;

public class EndUserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_user);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        menu.findItem(R.id.action_sendPushNotif).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                LoginActivity.LogOutOfApp(this);
                finish();
                return true;

            case R.id.action_settings:
                final SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("SharedPrefs", 0);

                List<String> categoriesTitles = new ArrayList<>();
                categoriesTitles.add((sharedPref.getBoolean("pushEnabled", true) ? "Disable" : "Enable") + " push notifications");

                AlertDialog.Builder b = new AlertDialog.Builder(this);
                b.setTitle("Settings");

                b.setItems(categoriesTitles.toArray(new String[]{}), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putBoolean("pushEnabled", !sharedPref.getBoolean("pushEnabled", true));
                        editor.apply();
                    }
                });

                b.show();
                return true;
            default:

                return super.onOptionsItemSelected(item);
        }
    }
}

package com.ccwiz.ccwiz.Activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ccwiz.ccwiz.Interfaces.OnDoneListener;
import com.ccwiz.ccwiz.Model.Question;
import com.ccwiz.ccwiz.R;
import com.ccwiz.ccwiz.Utils.RequestUtils;
import com.ccwiz.ccwiz.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class QuizActivity extends AppCompatActivity {

    //ProgressDialog progressDialog;
    TextView questionText;
    Button option1, option2, option3, option4, nextQuest;

    Question currentQuestion;

    int attempts = 0;

    long millisAtQuestionStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        questionText = (TextView) findViewById(R.id.questionText);
        option1 = (Button) findViewById(R.id.option1);
        option2 = (Button) findViewById(R.id.option2);
        option3 = (Button) findViewById(R.id.option3);
        option4 = (Button) findViewById(R.id.option4);
        nextQuest = (Button) findViewById(R.id.nextQ);

        option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Answer(0);
            }
        });
        option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Answer(1);
            }
        });
        option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Answer(2);
            }
        });
        option4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Answer(3);
            }
        });
        nextQuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadNewQuestion();
            }
        });

        loadNewQuestion();
    }

    void loadNewQuestion() {
        StringRequest getRequest = new StringRequest(Request.Method.POST, AppController.ServerName + "getQuestion",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        //progressDialog.hide();

                        try {
                            JSONObject object = new JSONObject(response);

                            if (!object.getBoolean("error")) {
                                JSONObject questionObject = object.getJSONObject("question");
                                currentQuestion = Question.parseJSON(questionObject.toString());

                                setViews();
                                attempts = 0;
                                millisAtQuestionStart = System.currentTimeMillis();
                            } else {
                                String message = object.getString("message");
                                Log.e("Volley", message);
                                OnErrorLoading(message);
                            }

                        } catch (JSONException e) {
                            Log.e("Volley", e.getMessage());
                            OnErrorLoading("Error!");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e("Volley", error.toString());
                        OnErrorLoading("Error!");
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", AppController.getInstance().mUser.getApiKey());

                return params;
            }
        };

        //progressDialog = ProgressDialog.show(this, "Loading question!", "Loading...");

        AppController.getInstance().addToRequestQueue(getRequest);
    }

    void setViews() {
        nextQuest.setVisibility(View.GONE);
        questionText.setText(currentQuestion.QuestionText);

        option1.setEnabled(true);
        option2.setEnabled(true);
        option3.setEnabled(true);
        option4.setEnabled(true);
        option1.setBackgroundResource(android.R.drawable.btn_default);
        option2.setBackgroundResource(android.R.drawable.btn_default);
        option3.setBackgroundResource(android.R.drawable.btn_default);
        option4.setBackgroundResource(android.R.drawable.btn_default);

        option1.setText(currentQuestion.Option1);
        option2.setText(currentQuestion.Option2);

        if (currentQuestion.AnswersCount() >= 3) {
            option3.setText(currentQuestion.Option3);
            option3.setVisibility(View.VISIBLE);
        } else
            option3.setVisibility(View.GONE);

        if (currentQuestion.AnswersCount() == 4) {
            option4.setText(currentQuestion.Option4);
            option4.setVisibility(View.VISIBLE);
        } else
            option4.setVisibility(View.GONE);
    }

    void Answer(int index) {
        attempts++;
        if (currentQuestion.RightOption == index) {
            switch (index) {
                case 0:
                    option1.setBackgroundColor(Color.GREEN);
                    break;
                case 1:
                    option2.setBackgroundColor(Color.GREEN);
                    break;
                case 2:
                    option3.setBackgroundColor(Color.GREEN);
                    break;
                case 3:
                    option4.setBackgroundColor(Color.GREEN);
                    break;
            }
            reportQuestion();
        } else {
            Toast.makeText(this, "Incorrect, try again!", Toast.LENGTH_LONG).show();
            switch (index) {
                case 0:
                    option1.setEnabled(false);
                    option1.setBackgroundColor(Color.RED);
                    break;
                case 1:
                    option2.setEnabled(false);
                    option2.setBackgroundColor(Color.RED);
                    break;
                case 2:
                    option3.setEnabled(false);
                    option3.setBackgroundColor(Color.RED);
                    break;
                case 3:
                    option4.setEnabled(false);
                    option4.setBackgroundColor(Color.RED);
                    break;
            }
        }
    }

    void reportQuestion() {
        Map<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");
        params.put("Authorization", AppController.getInstance().mUser.getApiKey());
        params.put("QuestionId", currentQuestion.Id + "");
        params.put("Attempts", attempts + "");
        params.put("Time", ((System.currentTimeMillis() - millisAtQuestionStart) / 1000) + "");

        RequestUtils.makeRequest(this, "report", params, new OnDoneListener() {
            @Override
            public void OnDone() {
                nextQuest.setVisibility(View.VISIBLE);
            }
        });
    }

    void OnErrorLoading(String message) {
        //progressDialog.hide();
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        finish();
    }
}

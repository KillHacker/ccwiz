package com.ccwiz.ccwiz.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.ccwiz.ccwiz.Fragments.LeaderBoard;
import com.ccwiz.ccwiz.Fragments.Questions;
import com.ccwiz.ccwiz.Fragments.Users;
import com.ccwiz.ccwiz.R;
import com.ccwiz.ccwiz.Utils.RequestUtils;
import com.ccwiz.ccwiz.app.AppController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private Fragment fragment;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Spinner spinnerForQuestions = (Spinner) findViewById(R.id.spinner);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);

        fragmentManager = getSupportFragmentManager();
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.menu_leaderboard:
                        fragment = new LeaderBoard();
                        spinnerForQuestions.setVisibility(View.GONE);
                        break;
                    case R.id.menu_users:
                        fragment = new Users();
                        spinnerForQuestions.setVisibility(View.GONE);
                        break;
                    case R.id.menu_questions:
                        fragment = new Questions();
                        spinnerForQuestions.setVisibility(View.VISIBLE);
                        break;
                }
                final FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.frame_layout, fragment).commit();
                return true;
            }
        });

        fragment = new LeaderBoard();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_layout, fragment).commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        menu.findItem(R.id.action_sendPushNotif).setVisible(AppController.getInstance().mUser.AccessLevel == 0);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                LoginActivity.LogOutOfApp(this);
                finish();
                return true;
            case R.id.action_sendPushNotif:
                final Context context = this;
                final Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.dialog_send_push_notification);
                dialog.setTitle("Send push notification");

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);

                final EditText titleView = (EditText) dialog.findViewById(R.id.titleView);
                final EditText messageView = (EditText) dialog.findViewById(R.id.messageView);
                Button sendButton = (Button) dialog.findViewById(R.id.sendButton);
                Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);

                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                sendButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String title = titleView.getText().toString();
                        String message = messageView.getText().toString();

                        if (title.isEmpty()) {
                            Toast.makeText(context, "Title is empty!", Toast.LENGTH_LONG).show();
                            return;
                        }

                        if (message.isEmpty()) {
                            Toast.makeText(context, "Message is empty!", Toast.LENGTH_LONG).show();
                            return;
                        }

                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json");
                        params.put("Authorization", AppController.getInstance().mUser.getApiKey());
                        params.put("Title", title);
                        params.put("Message", message);

                        RequestUtils.makeRequest(context, "sendPush", params);

                        dialog.hide();
                    }
                });

                dialog.show();
                return true;
            case R.id.action_settings:
                final SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("SharedPrefs", 0);

                List<String> categoriesTitles = new ArrayList<>();
                categoriesTitles.add((sharedPref.getBoolean("pushEnabled", true) ? "Disable" : "Enable") + " push notifications");

                AlertDialog.Builder b = new AlertDialog.Builder(this);
                b.setTitle("Settings");

                b.setItems(categoriesTitles.toArray(new String[]{}), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putBoolean("pushEnabled", !sharedPref.getBoolean("pushEnabled", true));
                        editor.apply();
                    }
                });

                b.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

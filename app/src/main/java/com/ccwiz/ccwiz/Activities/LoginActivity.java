package com.ccwiz.ccwiz.Activities;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ccwiz.ccwiz.Interfaces.OnDoneListener;
import com.ccwiz.ccwiz.Model.User;
import com.ccwiz.ccwiz.R;
import com.ccwiz.ccwiz.Utils.CustomRequest;
import com.ccwiz.ccwiz.Utils.PublicIP;
import com.ccwiz.ccwiz.Utils.RequestUtils;
import com.ccwiz.ccwiz.Utils.Utils;
import com.ccwiz.ccwiz.app.AppController;
import com.ccwiz.ccwiz.app.PushBackgroundService;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class LoginActivity extends AppCompatActivity implements OnDoneListener {

    Intent mServiceIntent;
    ProgressDialog progressDialog;
    TextView usernameView, passView;
    Button loginButton;
    private PushBackgroundService pushBackgroundService;

    public static void LogOutOfApp(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("SharedPrefs", 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.remove("mUser");
        sharedPreferencesEditor.apply();

        Map<String, String> params = new HashMap<String, String>();
        params.put("Content-Type", "application/json");
        params.put("Authorization", AppController.getInstance().mUser.getApiKey());
        params.put("sessionId", sharedPreferences.getInt("sessionId", -1) + "");

        RequestUtils.makeRequest(context, "endSession", params);

        AppController.getInstance().mUser = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameView = (TextView) findViewById(R.id.input_username);
        passView = (TextView) findViewById(R.id.input_password);
        loginButton = (Button) findViewById(R.id.btn_login);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Logging in!");

        pushBackgroundService = new PushBackgroundService(this);
        mServiceIntent = new Intent(this, pushBackgroundService.getClass());
        if (!isMyServiceRunning(pushBackgroundService.getClass())) {
            startService(mServiceIntent);
        }

        SharedPreferences sharedPreferences = getSharedPreferences("SharedPrefs", 0);
        if (sharedPreferences.contains("mUser")) {
            LogIntoApp();
        }

        loginButton.setEnabled(true);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLogin(v);
            }
        });
    }

    @Override
    public void OnDone() {
        LogIntoApp();
    }

    public void onLogin(final View view) {
        if (usernameView.getText().toString().isEmpty())
            Toast.makeText(this, "Fill your username!", Toast.LENGTH_LONG).show();
        else if (passView.getText().toString().isEmpty())
            Toast.makeText(this, "Fill your password!", Toast.LENGTH_LONG).show();
        else {
            StringRequest postRequest = new StringRequest(CustomRequest.Method.POST, AppController.ServerName + "login",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response
                            progressDialog.hide();
                            try {
                                JSONObject object = new JSONObject(response);

                                if (object.getBoolean("error")) {
                                    String message = object.getString("message");
                                    Log.e("Volley", message);
                                    Toast.makeText(view.getContext(), message, Toast.LENGTH_LONG).show();
                                } else {
                                    AppController.getInstance().mUser = User.parseJSON(object.getJSONObject("user").toString());

                                    SharedPreferences sharedPreferences = getSharedPreferences("SharedPrefs", 0);
                                    SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
                                    sharedPreferencesEditor.putString("mUser", object.getJSONObject("user").toString());
                                    sharedPreferencesEditor.putInt("sessionId", object.getInt("sessionId"));
                                    sharedPreferencesEditor.apply();

                                    LogIntoApp();
                                }
                            } catch (Exception e) {
                                Log.e("Volley", e.getMessage());
                                Toast.makeText(view.getContext(), "Error!", Toast.LENGTH_LONG).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.e("Volley", error.toString());
                            progressDialog.hide();
                            Toast.makeText(view.getContext(), "Error!", Toast.LENGTH_LONG).show();
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("username", usernameView.getText().toString());
                    params.put("password", passView.getText().toString());
                    params.put("ipAddress", PublicIP.get());
                    params.put("deviceInfo", Utils.getDeviceName());

                    return params;
                }
            };

            AppController.getInstance().addToRequestQueue(postRequest);

            progressDialog.show();
        }
    }

    void LogIntoApp() {
        if (AppController.getInstance().mUser == null)
            return;

        final Context context = this;
        //schedule the timer, to alert server that user is online
        new Timer().scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if (AppController.getInstance().mUser == null)
                    return;

                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", AppController.getInstance().mUser.getApiKey());

                RequestUtils.makeRequest(context, "imHere", params);
            }
        }, 2000, AppController.UpdatePeriodMillis);

        if (AppController.getInstance().mUser.AccessLevel < 2)
            startActivity(new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
        else
            startActivity(new Intent(this, EndUserActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));

        progressDialog.dismiss();
        finish();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    @Override
    protected void onDestroy() {
        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();
    }
}

package com.ccwiz.ccwiz.Utils;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ccwiz.ccwiz.Adapters.QuestionAdapter;
import com.ccwiz.ccwiz.Interfaces.OnDoneListener;
import com.ccwiz.ccwiz.Model.Question;
import com.ccwiz.ccwiz.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Enver on 22.03.2017..
 */

public class RequestUtils {

    public static void makeRequest(final Context context, String path, final Map<String, String> params) {
        StringRequest getRequest = new StringRequest(Request.Method.POST, AppController.ServerName + path,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        try {
                            JSONObject object = new JSONObject(response);

                            if (object.getBoolean("error")) {
                                String message = object.getString("message");
                                Log.e("Volley", message);
                                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            Log.e("Volley", e.toString());
                            Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e("Volley", error.toString());
                        Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(getRequest);
    }

    public static void makeRequest(final Context context, String path, final Map<String, String> params, final OnDoneListener onDoneListener) {
        StringRequest getRequest = new StringRequest(Request.Method.POST, AppController.ServerName + path,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //response
                        try {
                            JSONObject object = new JSONObject(response);

                            if (object.getBoolean("error")) {
                                String message = object.getString("message");
                                Log.e("Volley", message);
                                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                            } else
                                onDoneListener.OnDone();

                        } catch (JSONException e) {
                            Log.e("Volley", e.toString());
                            Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e("Volley", error.toString());
                        Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(getRequest);
    }

    public static void loadQuestions(final Context context, final ProgressBar progressBar, final List<Question> questionList, final QuestionAdapter questionAdapter) {
        StringRequest getRequest = new StringRequest(Request.Method.POST, AppController.ServerName + "allQuestions",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject object = new JSONObject(response);

                            if (!object.getBoolean("error")) {
                                JSONArray users = object.getJSONArray("questions");
                                questionList.clear();

                                for (int i = 0; i < users.length(); i++) {
                                    JSONObject questionObject = users.getJSONObject(i);
                                    Question question = Question.parseJSON(questionObject.toString());
                                    questionList.add(question);
                                }

                                questionAdapter.questionList = questionList;
                                questionAdapter.notifyDataSetChanged();
                            } else
                                OnErrorLoading(context, progressBar);

                        } catch (JSONException e) {
                            Log.e("Volley", e.getMessage());
                            OnErrorLoading(context, progressBar);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e("Volley", error.toString());
                        OnErrorLoading(context, progressBar);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", AppController.getInstance().mUser.getApiKey());

                return params;
            }
        };

        progressBar.setVisibility(View.VISIBLE);

        AppController.getInstance().addToRequestQueue(getRequest);
    }

    static void OnErrorLoading(Context context, ProgressBar progressBar) {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
    }
}

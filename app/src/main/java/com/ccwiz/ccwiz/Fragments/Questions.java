package com.ccwiz.ccwiz.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ccwiz.ccwiz.Adapters.QuestionAdapter;
import com.ccwiz.ccwiz.Interfaces.OnDoneListener;
import com.ccwiz.ccwiz.Model.Category;
import com.ccwiz.ccwiz.Model.CategoryType;
import com.ccwiz.ccwiz.Model.Question;
import com.ccwiz.ccwiz.R;
import com.ccwiz.ccwiz.Utils.RequestUtils;
import com.ccwiz.ccwiz.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Enver on 22.03.2017..
 */

public class Questions extends Fragment {

    final List<Question> questionList = new ArrayList<>();
    final List<Category> categoryList = new ArrayList<>();
    final List<Category> modelList = new ArrayList<>();
    final List<Category> technologyList = new ArrayList<>();
    View mView;
    Context context;
    Spinner spinner;
    FloatingActionButton fab;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    ListView listView;

    QuestionAdapter questionAdapter;
    ArrayAdapter<Category> categoryAdapter;
    ArrayAdapter<Category> modelAdapter;
    ArrayAdapter<Category> technologyAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_questions, container, false);
        context = getContext();

        fab = (FloatingActionButton) mView.findViewById(R.id.fab);
        spinner = (Spinner) getActivity().findViewById(R.id.spinner);
        listView = (ListView) mView.findViewById(R.id.listView);
        progressBar = (ProgressBar) mView.findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) mView.findViewById(R.id.recyclerView);

        questionAdapter = new QuestionAdapter(questionList, context, new OnDoneListener() {
            @Override
            public void OnDone() {
                RequestUtils.loadQuestions(context, progressBar, questionList, questionAdapter);
            }
        });

        recyclerView.setAdapter(questionAdapter);

        categoryAdapter = new CategoryAdapter(categoryList);
        modelAdapter = new CategoryAdapter(modelList);
        technologyAdapter = new CategoryAdapter(technologyList);

        setListViewOnClickListener();

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        setSpinner();

        return mView;
    }

    void setListViewOnClickListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                final List<Category> list;

                final int whichCMT;
                if (((Category) listView.getAdapter().getItem(0)).type == CategoryType.Category) {
                    whichCMT = 0;
                    list = categoryList;
                } else if (((Category) listView.getAdapter().getItem(0)).type == CategoryType.Model) {
                    whichCMT = 1;
                    list = modelList;
                } else {
                    whichCMT = 2;
                    list = technologyList;
                }

                final Category category = list.get(position);

                final CharSequence[] items;
                if (AppController.getInstance().mUser.AccessLevel == 0)
                    if (category.Enabled)
                        items = new CharSequence[]{"Edit title", "Remove", "Disable"};
                    else
                        items = new CharSequence[]{"Edit title", "Remove", "Enable"};
                else
                    items = new CharSequence[]{"Edit title", "Remove"};

                new AlertDialog.Builder(context)
                        .setTitle(list.get(position).toString())
                        .setItems(items, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        renameCMT(list, position, whichCMT);
                                        break;
                                    case 1:
                                        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                switch (which) {
                                                    case DialogInterface.BUTTON_POSITIVE:
                                                        //Yes button clicked
                                                        Map<String, String> params = new HashMap<>();
                                                        params.put("Content-Type", "application/json");
                                                        params.put("Authorization", AppController.getInstance().mUser.getApiKey());
                                                        params.put("Id", category.Id + "");

                                                        RequestUtils.makeRequest(context, whichCMT == 0 ? "removeCategory" : (whichCMT == 1 ? "removeModel" : "removeTechnology"), params, new OnDoneListener() {
                                                            @Override
                                                            public void OnDone() {
                                                                loadCMT(list, whichCMT);
                                                            }
                                                        });
                                                        break;

                                                    case DialogInterface.BUTTON_NEGATIVE:
                                                        dialog.cancel();
                                                        break;
                                                }
                                            }
                                        };

                                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                        builder.setMessage("All questions under this " + (whichCMT == 0 ? "category" : (whichCMT == 1 ? "model" : "technology")) + " will be removed! Are you sure?").setPositiveButton("Yes", dialogClickListener)
                                                .setNegativeButton("Cancel", dialogClickListener).show();

                                        break;
                                    case 2:
                                        //Enable/disable
                                        Map<String, String> params = new HashMap<>();
                                        params.put("Content-Type", "application/json");
                                        params.put("Authorization", AppController.getInstance().mUser.getApiKey());
                                        params.put("Id", category.Id + "");
                                        params.put("Enabled", !category.Enabled ? "1" : "0");

                                        RequestUtils.makeRequest(context, (whichCMT == 0 ? "enableCategory" : (whichCMT == 1 ? "enableModel" : "enableTechnology")), params, new OnDoneListener() {
                                            @Override
                                            public void OnDone() {
                                                loadCMT(list, whichCMT);
                                            }
                                        });
                                }
                            }
                        }).create().show();
            }
        });
    }

    void setSpinner() {
        List<String> spinnerItems = new ArrayList<>();
        spinnerItems.add("Questions");
        spinnerItems.add("Categories");
        spinnerItems.add("Models");
        spinnerItems.add("Technologies");

        final ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(context, R.layout.simple_spinner_dropdown_item_toolbar, spinnerItems);
        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item_white);
        spinner.setAdapter(spinnerAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        recyclerView.setVisibility(View.VISIBLE);
                        listView.setVisibility(View.GONE);
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                questionAdapter.PrepareEditQuestions(-1, true);
                            }
                        });
                        RequestUtils.loadQuestions(context, progressBar, questionList, questionAdapter);
                        break;
                    case 1:
                        recyclerView.setVisibility(View.GONE);
                        listView.setVisibility(View.VISIBLE);
                        listView.setAdapter(categoryAdapter);
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                addCMT(0);
                            }
                        });
                        loadCMT(categoryList, 0);
                        break;
                    case 2:
                        recyclerView.setVisibility(View.GONE);
                        listView.setVisibility(View.VISIBLE);
                        listView.setAdapter(modelAdapter);
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                addCMT(1);
                            }
                        });
                        loadCMT(modelList, 1);
                        break;
                    case 3:
                        recyclerView.setVisibility(View.GONE);
                        listView.setVisibility(View.VISIBLE);
                        listView.setAdapter(technologyAdapter);
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                addCMT(2);
                            }
                        });
                        loadCMT(technologyList, 2);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinner.setSelection(0);
    }

    void loadCMT(final List<Category> list, final int whichCMT) {
        StringRequest getRequest = new StringRequest(Request.Method.POST, AppController.ServerName + (whichCMT == 0 ? "allCategories" : (whichCMT == 1 ? "allModels" : "allTechnologies")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject object = new JSONObject(response);

                            if (!object.getBoolean("error")) {
                                JSONArray cmts = object.getJSONArray((whichCMT == 0 ? "categories" : (whichCMT == 1 ? "models" : "technologies")));
                                list.clear();

                                for (int i = 0; i < cmts.length(); i++) {
                                    JSONObject cmtObject = cmts.getJSONObject(i);
                                    Category category = Category.parseJSON(cmtObject.toString());
                                    if (whichCMT == 0)
                                        category.type = CategoryType.Category;
                                    else if (whichCMT == 1)
                                        category.type = CategoryType.Model;
                                    else
                                        category.type = CategoryType.Technology;

                                    list.add(category);
                                }

                                if (whichCMT == 0)
                                    categoryAdapter.notifyDataSetChanged();
                                else if (whichCMT == 1)
                                    modelAdapter.notifyDataSetChanged();
                                else
                                    technologyAdapter.notifyDataSetChanged();
                            } else {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            Log.e("Volley", e.getMessage());
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e("Volley", error.toString());
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", AppController.getInstance().mUser.getApiKey());

                return params;
            }
        };

        progressBar.setVisibility(View.VISIBLE);

        AppController.getInstance().addToRequestQueue(getRequest);
    }

    void renameCMT(final List<Category> list, final int position, final int whichCMT) {
        final EditText edt = new EditText(context);
        edt.setSingleLine();
        FrameLayout container = new FrameLayout(context);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
        params.rightMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
        edt.setLayoutParams(params);
        container.addView(edt);
        edt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        edt.setText(list.get(position).toString());
        final AlertDialog.Builder altBx = new AlertDialog.Builder(context);
        altBx.setTitle(list.get(position).toString());
        altBx.setMessage("Enter new " + (whichCMT == 0 ? "category" : (whichCMT == 1 ? "model" : "technology")) + " name:");
        altBx.setView(container);

        altBx.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String newTitle = edt.getText().toString();
                if (newTitle.isEmpty()) {
                    Toast.makeText(context, "Title can not be empty!", Toast.LENGTH_LONG).show();
                    return;
                }

                Category category = list.get(position);

                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", AppController.getInstance().mUser.getApiKey());
                params.put("Id", category.Id + "");
                params.put("NewTitle", newTitle);

                RequestUtils.makeRequest(context, whichCMT == 0 ? "renameCategory" : (whichCMT == 1 ? "renameModel" : "renameTechnology"), params, new OnDoneListener() {
                    @Override
                    public void OnDone() {
                        loadCMT(list, whichCMT);
                    }
                });
            }
        });
        altBx.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        altBx.show();
    }

    void addCMT(final int whichCMT) {
        final EditText edt = new EditText(context);
        edt.setSingleLine();
        FrameLayout container = new FrameLayout(context);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
        params.rightMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
        edt.setLayoutParams(params);
        container.addView(edt);
        edt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        final AlertDialog.Builder altBx = new AlertDialog.Builder(context);
        altBx.setTitle("New " + (whichCMT == 0 ? "category" : (whichCMT == 1 ? "model" : "technology")));
        altBx.setMessage("Enter new " + (whichCMT == 0 ? "category" : (whichCMT == 1 ? "model" : "technology")) + " name:");
        altBx.setView(container);

        altBx.setPositiveButton("CREATE", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String newTitle = edt.getText().toString();
                if (newTitle.isEmpty()) {
                    Toast.makeText(context, "Title can not be empty!", Toast.LENGTH_LONG).show();
                    return;
                }

                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", AppController.getInstance().mUser.getApiKey());
                params.put("Title", newTitle);

                RequestUtils.makeRequest(context, whichCMT == 0 ? "addCategory" : (whichCMT == 1 ? "addModel" : "addTechnology"), params, new OnDoneListener() {
                    @Override
                    public void OnDone() {
                        loadCMT((whichCMT == 0 ? categoryList : (whichCMT == 1 ? modelList : technologyList)), whichCMT);
                    }
                });
            }
        });
        altBx.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        altBx.show();
    }

    class CategoryAdapter extends ArrayAdapter<Category> {
        List<Category> mObjects;

        public CategoryAdapter(List<Category> objects) {
            super(context, android.R.layout.simple_list_item_1, objects);
            mObjects = objects;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);

            if (AppController.getInstance().mUser.AccessLevel == 0)
                if (!mObjects.get(position).Enabled)
                    view.setBackgroundColor(getResources().getColor(R.color.disabled_white));
                else
                    view.setBackgroundColor(getResources().getColor(R.color.white));

            return view;
        }
    }
}

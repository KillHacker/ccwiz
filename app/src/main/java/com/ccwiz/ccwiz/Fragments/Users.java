package com.ccwiz.ccwiz.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ccwiz.ccwiz.Adapters.UserAdapterInUsers;
import com.ccwiz.ccwiz.Interfaces.OnDoneListener;
import com.ccwiz.ccwiz.Model.User;
import com.ccwiz.ccwiz.R;
import com.ccwiz.ccwiz.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Enver on 22.03.2017..
 */

public class Users extends Fragment {

    final List<User> userList = new ArrayList<>();
    View mView;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    UserAdapterInUsers userAdapterInUsers;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_users, container, false);

        progressBar = (ProgressBar) mView.findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) mView.findViewById(R.id.recyclerView);

        userAdapterInUsers = new UserAdapterInUsers(userList, getContext(), new OnDoneListener() {
            @Override
            public void OnDone() {
                loadUsers();
            }
        });
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(userAdapterInUsers);

        FloatingActionButton fab = (FloatingActionButton) mView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userAdapterInUsers.editUser(-1, true);
            }
        });

        loadUsers();

        return mView;
    }

    public void loadUsers() {
        StringRequest getRequest = new StringRequest(Request.Method.POST, AppController.ServerName + "allUsers",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject object = new JSONObject(response);

                            if (!object.getBoolean("error")) {
                                JSONArray users = object.getJSONArray("users");
                                userList.clear();

                                for (int i = 0; i < users.length(); i++) {
                                    JSONObject userObject = users.getJSONObject(i);
                                    User user = User.parseJSON(userObject.toString());
                                    userList.add(user);
                                }

                                userAdapterInUsers.userList = userList;
                                userAdapterInUsers.notifyDataSetChanged();
                            } else
                                OnErrorLoading();

                        } catch (JSONException e) {
                            Log.e("Volley", e.getMessage());
                            OnErrorLoading();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e("Volley", error.toString());
                        OnErrorLoading();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", AppController.getInstance().mUser.getApiKey());

                return params;
            }
        };

        progressBar.setVisibility(View.VISIBLE);

        AppController.getInstance().addToRequestQueue(getRequest);
    }

    void OnErrorLoading() {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(mView.getContext(), "Error!", Toast.LENGTH_LONG).show();
    }
}

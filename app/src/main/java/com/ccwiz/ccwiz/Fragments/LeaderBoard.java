package com.ccwiz.ccwiz.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ccwiz.ccwiz.Activities.QuizActivity;
import com.ccwiz.ccwiz.Adapters.UserAdapter;
import com.ccwiz.ccwiz.Model.User;
import com.ccwiz.ccwiz.R;
import com.ccwiz.ccwiz.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Enver on 20.03.2017..
 */

public class LeaderBoard extends Fragment {

    final List<User> userList = new ArrayList<>();
    View mView;
    Button playQuizButton;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    UserAdapter userAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_leaderboard, container, false);

        playQuizButton = (Button) mView.findViewById(R.id.playQuizButton);
        progressBar = (ProgressBar) mView.findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) mView.findViewById(R.id.recyclerView);

        userAdapter = new UserAdapter(userList, getContext(), true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(userAdapter);

        playQuizButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), QuizActivity.class));
            }
        });

        loadLeaderboard();

        return mView;
    }

    public void loadLeaderboard() {
        StringRequest postRequest = new StringRequest(Request.Method.POST, AppController.ServerName + "leaderboard",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject object = new JSONObject(response);

                            if (!object.getBoolean("error")) {
                                JSONArray users = object.getJSONArray("users");
                                userList.clear();

                                for (int i = 0; i < users.length(); i++) {
                                    JSONObject userObject = users.getJSONObject(i);
                                    User user = User.parseJSON(userObject.toString());
                                    userList.add(user);
                                }

                                userAdapter.userList = userList;
                                userAdapter.notifyDataSetChanged();
                            } else
                                OnErrorLoading();

                        } catch (JSONException e) {
                            Log.e("Volley", e.getMessage());
                            OnErrorLoading();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e("Volley", error.toString());
                        OnErrorLoading();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", AppController.getInstance().mUser.getApiKey());

                return params;
            }
        };

        progressBar.setVisibility(View.VISIBLE);

        AppController.getInstance().addToRequestQueue(postRequest);
    }

    void OnErrorLoading() {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(mView.getContext(), "Error!", Toast.LENGTH_LONG).show();
    }
}

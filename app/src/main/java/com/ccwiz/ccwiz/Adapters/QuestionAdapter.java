package com.ccwiz.ccwiz.Adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ccwiz.ccwiz.Interfaces.OnDoneListener;
import com.ccwiz.ccwiz.Model.Category;
import com.ccwiz.ccwiz.Model.CategoryType;
import com.ccwiz.ccwiz.Model.Question;
import com.ccwiz.ccwiz.R;
import com.ccwiz.ccwiz.Utils.RequestUtils;
import com.ccwiz.ccwiz.Utils.Utils;
import com.ccwiz.ccwiz.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Enver on 22.03.2017..
 */

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.ViewHolder> {

    public List<Question> questionList;
    Context context;
    OnDoneListener onDoneListener;

    public QuestionAdapter(List<Question> questionList, Context context, OnDoneListener onDoneListener) {
        this.questionList = questionList;
        this.context = context;
        this.onDoneListener = onDoneListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_question_in_questions, parent, false);
        ViewHolder viewHolder = new ViewHolder(v, this);
        return viewHolder;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Question question = questionList.get(position);

        holder.questionTextView.setText(question.QuestionText);
        holder.rightAnswerView.setText("Right answer: " + question.getRightOption());
        holder.difficultyLevelView.setText("Difficulty level: " + question.DifficultyLevel);
        holder.categoryView.setText("Category: " + question.CategoryTitle);
        holder.modifiedView.setText("Last modified: " + question.DateModified);
    }

    @Override
    public int getItemCount() {
        return questionList.size();
    }

    public boolean onLongClickHandler(final RecyclerView.ViewHolder viewHolder) {

        CharSequence[] items = {"Edit Question", "Remove question"};
        new AlertDialog.Builder(context)
                .setTitle("Question")
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                //Edit question
                                PrepareEditQuestions(viewHolder.getAdapterPosition(), false);
                                break;
                            case 1:
                                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case DialogInterface.BUTTON_POSITIVE:
                                                //Yes button clicked

                                                Question question = questionList.get(viewHolder.getAdapterPosition());

                                                Map<String, String> params = new HashMap<String, String>();
                                                params.put("Content-Type", "application/json");
                                                params.put("Authorization", AppController.getInstance().mUser.getApiKey());
                                                params.put("QuestionId", question.Id + "");

                                                RequestUtils.makeRequest(context, "removeQuestion", params, onDoneListener);
                                                break;

                                            case DialogInterface.BUTTON_NEGATIVE:
                                                //No button clicked
                                                break;
                                        }
                                    }
                                };

                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                                        .setNegativeButton("Cancel", dialogClickListener).show();

                                break;
                        }
                    }
                }).create().show();

        return true;
    }

    /**
     * If making new question, put questionAdapterPosition for example -1, it doesn't matter
     */
    public void PrepareEditQuestions(final int questionAdapterPosition, final boolean newQuestion) {
        final ProgressDialog progressDialog = new ProgressDialog(context);

        StringRequest postRequest = new StringRequest(Request.Method.POST, AppController.ServerName + "editQuestionSet",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        progressDialog.hide();

                        try {
                            JSONObject object = new JSONObject(response);

                            if (!object.getBoolean("error")) {
                                JSONArray categories = object.getJSONArray("categories");

                                final List<Category> categoriesList = new ArrayList<>();

                                for (int i = 0; i < categories.length(); i++) {
                                    JSONObject categoryObject = categories.getJSONObject(i);
                                    Category category = Category.parseJSON(categoryObject.toString());
                                    category.type = CategoryType.Category;
                                    categoriesList.add(category);
                                }

                                JSONArray models = object.getJSONArray("models");

                                final List<Category> modelList = new ArrayList<>();

                                for (int i = 0; i < models.length(); i++) {
                                    JSONObject modelObject = models.getJSONObject(i);
                                    Category model = Category.parseJSON(modelObject.toString());
                                    model.type = CategoryType.Model;
                                    modelList.add(model);
                                }

                                JSONArray technologies = object.getJSONArray("technologies");

                                final List<Category> technologyList = new ArrayList<>();

                                for (int i = 0; i < technologies.length(); i++) {
                                    JSONObject technologyObject = technologies.getJSONObject(i);
                                    Category technology = Category.parseJSON(technologyObject.toString());
                                    technology.type = CategoryType.Technology;
                                    technologyList.add(technology);
                                }

                                ShowEditDialog(questionAdapterPosition, categoriesList, modelList, technologyList, newQuestion);
                            } else
                                OnErrorLoading(progressDialog);

                        } catch (JSONException e) {
                            Log.e("Volley", e.getMessage());
                            OnErrorLoading(progressDialog);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e("Volley", error.toString());
                        OnErrorLoading(progressDialog);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", AppController.getInstance().mUser.getApiKey());

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(postRequest);

        progressDialog.show();
    }

    void OnErrorLoading(ProgressDialog progressDialog) {
        progressDialog.hide();
        Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
    }

    public void ShowEditDialog(int questionAdapterPosition, final List<Category> categories, final List<Category> models, final List<Category> technologies, final boolean newQuestion) {
        final Question question;

        if (!newQuestion)
            question = questionList.get(questionAdapterPosition);
        else
            question = new Question();

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_edit_question);
        dialog.setTitle(newQuestion ? "Add question" : "Edit question");

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        //Getting

        final EditText questionText = (EditText) dialog.findViewById(R.id.questionText);

        final RadioButton option1 = (RadioButton) dialog.findViewById(R.id.firstOption);
        final RadioButton option2 = (RadioButton) dialog.findViewById(R.id.secondOption);
        final RadioButton option3 = (RadioButton) dialog.findViewById(R.id.thirdOption);
        final RadioButton option4 = (RadioButton) dialog.findViewById(R.id.forthOption);

        Utils.setRadioExclusiveClick((ViewGroup) dialog.findViewById(R.id.dialogRoot));

        final EditText option1Text = (EditText) dialog.findViewById(R.id.option1Text);
        final EditText option2Text = (EditText) dialog.findViewById(R.id.option2Text);
        final EditText option3Text = (EditText) dialog.findViewById(R.id.option3Text);
        final EditText option4Text = (EditText) dialog.findViewById(R.id.option4Text);

        final SeekBar difficultyBar = (SeekBar) dialog.findViewById(R.id.difficultyLevel);

        final Spinner categorySpinner = (Spinner) dialog.findViewById(R.id.categorySpinner);
        final Spinner modelSpinner = (Spinner) dialog.findViewById(R.id.modelSpinner);
        final Spinner technologySpinner = (Spinner) dialog.findViewById(R.id.technologySpinner);

        final TextView dateAdded = (TextView) dialog.findViewById(R.id.addedView);
        TextView dateModified = (TextView) dialog.findViewById(R.id.modifiedView);

        Button saveButton = (Button) dialog.findViewById(R.id.saveButton);
        Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);

        //Setting

        difficultyBar.setMax(0);
        difficultyBar.setMax(4);

        //Category
        final ArrayAdapter<Category> categoryArrayAdapter =
                new ArrayAdapter<Category>(context, R.layout.simple_spinner_dropdown_item, categories);
        categoryArrayAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(categoryArrayAdapter);
        int currentCategory = 0;

        //Model
        ArrayAdapter<Category> modelArrayAdapter =
                new ArrayAdapter<Category>(context, R.layout.simple_spinner_dropdown_item, models);
        modelArrayAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        modelSpinner.setAdapter(modelArrayAdapter);
        int currentModel = 0;

        //Technology
        ArrayAdapter<Category> technologyArrayAdapter =
                new ArrayAdapter<Category>(context, R.layout.simple_spinner_dropdown_item, technologies);
        technologyArrayAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        technologySpinner.setAdapter(technologyArrayAdapter);
        int currentTechnology = 0;

        if (!newQuestion) {
            questionText.setText(question.QuestionText);

            switch (question.RightOption) {
                case 0:
                    option1.setChecked(true);
                    break;
                case 1:
                    option2.setChecked(true);
                    break;
                case 2:
                    option3.setChecked(true);
                    break;
                case 3:
                    option4.setChecked(true);
                    break;
            }

            option1Text.setText(question.Option1);
            option2Text.setText(question.Option2);
            option3Text.setText(question.Option3);
            option4Text.setText(question.Option4);

            difficultyBar.setProgress(question.DifficultyLevel - 1);

            for (int i = 0; i < categories.size(); i++)
                if (categories.get(i).Id == question.CategoryId) {
                    currentCategory = i;
                    break;
                }

            for (int i = 0; i < models.size(); i++)
                if (models.get(i).Id == question.ModelId) {
                    currentModel = i;
                    break;
                }

            for (int i = 0; i < technologies.size(); i++)
                if (technologies.get(i).Id == question.TechnologyId) {
                    currentTechnology = i;
                    break;
                }

            dateAdded.setText("Date added: " + question.DateAdded);
            dateModified.setText("Date modified: " + question.DateModified);
        } else {
            dateAdded.setVisibility(View.GONE);
            dateModified.setVisibility(View.GONE);
        }

        categorySpinner.setSelection(currentCategory);
        modelSpinner.setSelection(currentModel);
        technologySpinner.setSelection(currentTechnology);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int rightOption;
                if (option4.isChecked())
                    rightOption = 3;
                else if (option3.isChecked())
                    rightOption = 2;
                else if (option2.isChecked())
                    rightOption = 1;
                else
                    rightOption = 0;

                if (questionText.getText().toString().isEmpty()) {
                    Toast.makeText(context, "Question text is empty!", Toast.LENGTH_LONG).show();
                    return;
                }

                if (option1Text.getText().toString().isEmpty()) {
                    Toast.makeText(context, "Option 1 is empty! There must be at least 2 answers.", Toast.LENGTH_LONG).show();
                    return;
                }

                if (option2Text.getText().toString().isEmpty()) {
                    Toast.makeText(context, "Option 2 is empty! There must be at least 2 answers.", Toast.LENGTH_LONG).show();
                    return;
                }

                if ((rightOption == 2 && option3Text.getText().toString().isEmpty()) || (rightOption == 3 && option4Text.getText().toString().isEmpty())) {
                    Toast.makeText(context, "Select right answer!", Toast.LENGTH_LONG).show();
                    return;
                }

                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", AppController.getInstance().mUser.getApiKey());

                if (!newQuestion)
                    params.put("Id", question.Id + "");

                params.put("CategoryId", categories.get(categorySpinner.getSelectedItemPosition()).Id + "");
                params.put("ModelId", models.get(modelSpinner.getSelectedItemPosition()).Id + "");
                params.put("TechnologyId", technologies.get(technologySpinner.getSelectedItemPosition()).Id + "");
                params.put("DifficultyLevel", (difficultyBar.getProgress() + 1) + "");
                params.put("QuestionText", questionText.getText().toString());
                params.put("Option1", option1Text.getText().toString());
                params.put("Option2", option2Text.getText().toString());
                params.put("Option3", option3Text.getText().toString().isEmpty() ? "NULL" : option3Text.getText().toString());
                params.put("Option4", option4Text.getText().toString().isEmpty() ? "NULL" : option4Text.getText().toString());

                params.put("RightOption", rightOption + "");

                if (!newQuestion)
                    RequestUtils.makeRequest(context, "editQuestion", params, onDoneListener);
                else
                    RequestUtils.makeRequest(context, "addQuestion", params, onDoneListener);

                dialog.hide();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        TextView questionTextView, rightAnswerView, difficultyLevelView, categoryView, modifiedView;

        QuestionAdapter questionAdapter;

        public ViewHolder(View itemView, QuestionAdapter questionAdapter) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            this.questionAdapter = questionAdapter;

            questionTextView = (TextView) itemView.findViewById(R.id.questionText);
            rightAnswerView = (TextView) itemView.findViewById(R.id.rightAnswer);
            difficultyLevelView = (TextView) itemView.findViewById(R.id.difficultyLevel);
            categoryView = (TextView) itemView.findViewById(R.id.categoryText);
            modifiedView = (TextView) itemView.findViewById(R.id.modifiedText);
        }

        @Override
        public void onClick(View v) {
            questionAdapter.onLongClickHandler(this);
        }

        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    }
}
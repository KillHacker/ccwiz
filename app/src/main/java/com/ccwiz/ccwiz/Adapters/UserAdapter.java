package com.ccwiz.ccwiz.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ccwiz.ccwiz.Model.User;
import com.ccwiz.ccwiz.R;

import java.util.List;

/**
 * Created by Enver on 22.03.2017..
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    public List<User> userList;
    Context context;
    boolean leaderboard;

    public UserAdapter(List<User> userList, Context context, boolean leaderboard) {
        this.userList = userList;
        this.context = context;
        this.leaderboard = leaderboard;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_user_in_leaderboard, parent, false);
        ViewHolder viewHolder = new ViewHolder(v, this);
        return viewHolder;
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = userList.get(position);

        holder.usernameView.setText((showNumberBeforeUsername() ? (position + 1) + ". " : "") + user.Username);

        if (leaderboard) {
            holder.rightAnsweredQuestionsView.setText("" + user.Score);
            holder.successRateView.setText(user.QuestionCount > 0 ? (user.Score * 100 / user.QuestionCount) + "%" : "N/A");
            holder.averageTimeView.setText(user.AverageTime > 0 ? user.AverageTime + "s" : "N/A");
        } else {
            holder.rightAnsweredQuestionsView.setText("Right answered questions: " + user.Score);
            holder.successRateView.setText("Success rate: " + (user.QuestionCount > 0 ? (user.Score * 100 / user.QuestionCount) + "%" : "N/A"));
            holder.averageTimeView.setText("Average time: " + (user.AverageTime > 0 ? user.AverageTime + "s" : "N/A"));
        }
    }

    public boolean showNumberBeforeUsername() {
        return true;
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public boolean onLongClickHandler(RecyclerView.ViewHolder viewHolder) {
        return false;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        TextView usernameView, rightAnsweredQuestionsView, successRateView, averageTimeView;
        View onlineIndicator;

        UserAdapter userAdapter;

        public ViewHolder(View itemView, UserAdapter userAdapter) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            this.userAdapter = userAdapter;

            usernameView = (TextView) itemView.findViewById(R.id.userNameView);
            rightAnsweredQuestionsView = (TextView) itemView.findViewById(R.id.rightAnsweredQuestions);
            successRateView = (TextView) itemView.findViewById(R.id.successRate);
            averageTimeView = (TextView) itemView.findViewById(R.id.averageTime);
            onlineIndicator = itemView.findViewById(R.id.onlineIndicator);
        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public boolean onLongClick(View v) {
            return userAdapter.onLongClickHandler(this);
        }
    }
}
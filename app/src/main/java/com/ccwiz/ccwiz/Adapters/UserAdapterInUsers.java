package com.ccwiz.ccwiz.Adapters;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ccwiz.ccwiz.Interfaces.OnDoneListener;
import com.ccwiz.ccwiz.Model.Category;
import com.ccwiz.ccwiz.Model.User;
import com.ccwiz.ccwiz.R;
import com.ccwiz.ccwiz.Utils.RequestUtils;
import com.ccwiz.ccwiz.Utils.Utils;
import com.ccwiz.ccwiz.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Enver on 22.03.2017..
 */

public class UserAdapterInUsers extends UserAdapter {

    OnDoneListener onDoneListener;

    public UserAdapterInUsers(List<User> userList, Context context, OnDoneListener onDoneListener) {
        super(userList, context, false);
        this.onDoneListener = onDoneListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_user_in_users, parent, false);
        ViewHolder viewHolder = new ViewHolder(v, this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        User user = userList.get(position);

        holder.onlineIndicator.setVisibility(user.OnlineAgo < 3000 ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean showNumberBeforeUsername() {
        return false;
    }

    @Override
    public boolean onLongClickHandler(final RecyclerView.ViewHolder viewHolder) {

        final User user = userList.get(viewHolder.getAdapterPosition());

        //Dialog
        CharSequence[] items;
        if (AppController.getInstance().mUser.AccessLevel != 0)
            items = new CharSequence[]{"Edit user", "Remove user"};
        else
            items = new CharSequence[]{"Edit user", "Remove user", "Block category for user", "Unblock category for user",};

        new AlertDialog.Builder(context)
                .setTitle(user.Username)
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (user.AccessLevel == 0 && AppController.getInstance().mUser.AccessLevel != 0)
                            which++;

                        switch (which) {
                            case 0:
                                //Edit user
                                editUser(viewHolder.getAdapterPosition(), false);
                                break;
                            case 1:
                                //Remove user
                                removeUser(user);
                                break;
                            case 2:
                                //Block category for user
                                loadCategories(user.Id, true);
                                break;
                            case 3:
                                //Unblock category for user
                                loadCategories(user.Id, false);
                                break;
                        }
                    }
                }).create().show();

        return true;
    }

    /**
     * If making new user, put userAdapterPosition for example -1, it doesn't matter
     */
    public void editUser(final int userAdapterPosition, final boolean newUser) {
        final User user;

        if (!newUser)
            user = userList.get(userAdapterPosition);
        else
            user = new User();

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_edit_user);
        dialog.setTitle(newUser ? "Add user" : "Edit user");

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        //Getting

        final EditText userNameView = (EditText) dialog.findViewById(R.id.userNameView);

        final RadioButton adminOption = (RadioButton) dialog.findViewById(R.id.adminOption);
        final RadioButton superuserOption = (RadioButton) dialog.findViewById(R.id.superuserOption);
        final RadioButton userOption = (RadioButton) dialog.findViewById(R.id.userOption);

        final EditText emailView = (EditText) dialog.findViewById(R.id.emailView);
        final EditText passwordView = (EditText) dialog.findViewById(R.id.passwordView);

        final SeekBar maxDifficultyLevel = (SeekBar) dialog.findViewById(R.id.maxDifficultyLevel);

        Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);
        Button saveButton = (Button) dialog.findViewById(R.id.saveButton);

        User mUser = AppController.getInstance().mUser;
        if (mUser.AccessLevel == 0) {
            adminOption.setVisibility(View.VISIBLE);
        } else {
            adminOption.setVisibility(View.GONE);
        }

        maxDifficultyLevel.setMax(0);
        maxDifficultyLevel.setMax(4);

        if (!newUser) {
            userNameView.setText(user.Username);
            userNameView.setEnabled(false);

            maxDifficultyLevel.setProgress(user.MaxDifficultyLevel - 1);

            switch (user.AccessLevel) {
                case 0:
                    adminOption.setChecked(true);
                    break;
                case 1:
                    superuserOption.setChecked(true);
                    break;
                case 2:
                    userOption.setChecked(true);
                    break;
            }

            emailView.setText(user.Email);
            passwordView.setTypeface(emailView.getTypeface());
            passwordView.setText("NotChanged");
        }

        passwordView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && passwordView.getText().toString().equals("NotChanged"))
                    passwordView.setText("");
                else if (!hasFocus && passwordView.getText().toString().isEmpty() && !newUser)
                    passwordView.setText("NotChanged");
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (userNameView.getText().toString().isEmpty()) {
                    Toast.makeText(context, "Username is empty!", Toast.LENGTH_LONG).show();
                    return;
                }

                if (!emailView.getText().toString().isEmpty() && !Utils.isValidEmail(emailView.getText().toString())) {
                    Toast.makeText(context, "Email is not valid!", Toast.LENGTH_LONG).show();
                    return;
                }

                if (passwordView.getText().toString().isEmpty()) {
                    Toast.makeText(context, "Password is empty!", Toast.LENGTH_LONG).show();
                    return;
                }

                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", AppController.getInstance().mUser.getApiKey());

                if (!newUser)
                    params.put("Id", user.Id + "");

                int accessLevel = 2;
                if (superuserOption.isChecked())
                    accessLevel = 1;
                else if (adminOption.isChecked())
                    accessLevel = 0;

                if (newUser)
                    params.put("Username", userNameView.getText().toString());

                params.put("AccessLevel", accessLevel + "");
                params.put("Email", emailView.getText().toString().isEmpty() ? "NULL" : emailView.getText().toString());
                params.put("Password", passwordView.getText().toString());
                params.put("MaxDifficultyLevel", (maxDifficultyLevel.getProgress() + 1) + "");

                if (!newUser)
                    RequestUtils.makeRequest(context, "editUser", params, onDoneListener);
                else
                    RequestUtils.makeRequest(context, "addUser", params, onDoneListener);

                dialog.hide();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    void removeUser(final User user) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked

                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        params.put("Authorization", AppController.getInstance().mUser.getApiKey());
                        params.put("userId", user.Id + "");

                        RequestUtils.makeRequest(context, "removeUser", params, onDoneListener);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("Cancel", dialogClickListener).show();
    }

    void loadCategories(final int userId, final boolean toBlock) {
        final ProgressDialog progressDialog = new ProgressDialog(context);

        StringRequest getRequest = new StringRequest(Request.Method.POST, AppController.ServerName + (toBlock ? "categoriesToBlock" : "categoriesToUnblock"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        progressDialog.hide();

                        try {
                            JSONObject object = new JSONObject(response);

                            if (!object.getBoolean("error")) {
                                showCategoryDialog(object, userId, toBlock);
                            } else
                                OnErrorLoading(progressDialog);

                        } catch (JSONException e) {
                            Log.e("Volley", e.getMessage());
                            OnErrorLoading(progressDialog);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e("Volley", error.toString());
                        OnErrorLoading(progressDialog);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", AppController.getInstance().mUser.getApiKey());
                params.put("userId", userId + "");

                return params;
            }
        };

        progressDialog.show();
        AppController.getInstance().addToRequestQueue(getRequest);
    }

    void showCategoryDialog(JSONObject object, final int userId, final boolean toBlock) throws JSONException {
        JSONArray categories = object.getJSONArray("categories");

        final List<Category> categoriesToBLock = new ArrayList<>();
        List<String> categoriesTitles = new ArrayList<>();

        for (int i = 0; i < categories.length(); i++) {
            JSONObject categoryObject = categories.getJSONObject(i);
            Category category = Category.parseJSON(categoryObject.toString());
            categoriesToBLock.add(category);
            categoriesTitles.add(category.Title);
        }

        AlertDialog.Builder b = new AlertDialog.Builder(context);
        b.setTitle(toBlock ? "Categories to block" : "Categories to unblock");

        b.setItems(categoriesTitles.toArray(new String[]{}), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                int categoryId = categoriesToBLock.get(which).Id;

                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", AppController.getInstance().mUser.getApiKey());
                params.put("userId", userId + "");
                params.put("categoryId", categoryId + "");
                RequestUtils.makeRequest(context, toBlock ? "blockForUser" : "unblockForUser", params);
            }
        });

        b.show();
    }

    void OnErrorLoading(ProgressDialog progressDialog) {
        progressDialog.hide();
        Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
    }
}
